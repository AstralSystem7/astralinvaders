# AstralInvaders

Este projeto foi feito como Capstone da matéria de Programação 3 na Jala University. O projeto foi feito sobre orientação do professor José Rodrigues e tutor Daniel Linhares Lim Apo

## Começando

Estas instruções vão te dar uma cópia do projeto instalado e rodando na sua máquina local para fins de desenvolvimento e testes. Veja as notas sobre como implantar o projeto em um sistema ao vivo.

### Pré-requisitos

Antes de começar, você precisará ter o Unity instalado na sua máquina. Este projeto é compatível com:

Unity 2022.3

Certifique-se de ter a versão correta do Unity para evitar problemas de compatibilidade com o projeto.

### Instalação

Para configurar este projeto localmente, siga estas etapas:

1. **Clone o Repositório**

   Use o Git para clonar o repositório do projeto para a sua máquina local:

git clone https://gitlab.com/AstralSystem7/astralinvaders.git


2. **Abrindo o Projeto no Unity**

- Inicie o Unity Hub.
- Na aba "Projects", clique em "ADD".
- Navegue até o diretório onde você clonou o projeto e selecione a pasta do projeto.
- Certifique-se de que a versão do Unity está definida como 2022.3.
- Clique no projeto que agora aparece na lista para abri-lo com a versão correta do Unity.

## Usando o Projeto

Assim que abir o projeto com o Unity, é só clicar no play na parte de cima e pronto.
