using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public GameObject controlsPanel; // Referência para o painel de controles

    public void StartGame()
    {
        // Carrega a cena do jogo 
        SceneManager.LoadScene("SpaceInvaders");
    }

    public void ShowControls()
    {
        // Ativa o painel de controles
        if (controlsPanel != null)
            controlsPanel.SetActive(true);
    }
    
    public void HideControls()
    {
        if (controlsPanel != null)
            controlsPanel.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
    
    

}