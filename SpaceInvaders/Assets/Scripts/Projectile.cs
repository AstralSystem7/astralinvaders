using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float moveSpeed;
    public GameObject explosionPrefab;
    private PointManager _pointManager;
    private ProjectileShoot projectileShootScript; 

    void Start()
    {
        _pointManager = GameObject.Find("PointManager").GetComponent<PointManager>();
    }

    void Update()
    {
        transform.Translate(Vector2.up * (moveSpeed * Time.deltaTime));
    }

    public void SetParent(ProjectileShoot script) 
    {
        projectileShootScript = script;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            Destroy(collision.gameObject);

            // Atualiza a pontuação baseada no tipo de inimigo
            if (collision.gameObject.name == "Ship1")
            {
                _pointManager.UpdateScore(10);
            }
            else if (collision.gameObject.name == "Ship2")
            {
                _pointManager.UpdateScore(20);
            }
            else if (collision.gameObject.name == "Ship3")
            {
                _pointManager.UpdateScore(40);
            }
            else
            {
                _pointManager.UpdateScore(Random.Range(20, 200));
            }

            projectileShootScript.ClearProjectile(); // Notifica o script pai que o projétil foi destruído
            Destroy(gameObject);
        }
        else if (collision.gameObject.tag == "Boundary")
        {
            projectileShootScript.ClearProjectile(); // Notifica o script pai que o projétil foi destruído
            Destroy(gameObject);
        }
    }
}