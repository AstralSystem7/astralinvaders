using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject shipsPrefab;
    public GameObject currentShipsInstance;
    public float initialSpeed = 0.25f; // A velocidade inicial dos inimigos
    public float speedIncrease = 0.5f;
    private float currentSpeedIncrease = 0f;

    private void Start()
    {
        SpawnShips();
    }

    private void Update()
    {
        if (AreAllAliensDestroyed())
        {
            Destroy(currentShipsInstance);
            currentSpeedIncrease += speedIncrease;
            SpawnShips();
        }
    }

    private void SpawnShips()
    {
        currentShipsInstance = Instantiate(shipsPrefab, shipsPrefab.transform.position, Quaternion.identity);
        IncreaseShipsSpeed();
    }

    private bool AreAllAliensDestroyed()
    {
        return currentShipsInstance.transform.childCount == 0;
    }

    private void IncreaseShipsSpeed()
    {
        var shipMovement = currentShipsInstance.GetComponent<ShipMovement>();
        if (shipMovement != null)
        {
            shipMovement.movespeed = initialSpeed + currentSpeedIncrease;
        }
    }

    public void ResetLevel()
    {
        currentSpeedIncrease = 0f; // Reseta o aumento de velocidade
        SpawnShips(); // Respawn dos inimigos
    }
}

