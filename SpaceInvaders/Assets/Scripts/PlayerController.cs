using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float movespeed = 5;

    public float hInput;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Pega o input pelo Unity input manager
        hInput = Input.GetAxisRaw("Horizontal");
        
        transform.Translate(Vector2.right * (hInput * (Time.deltaTime * movespeed)));
    }
}
