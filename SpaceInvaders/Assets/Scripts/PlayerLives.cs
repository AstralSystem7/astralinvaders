using UnityEngine;
using UnityEngine.UI;

public class PlayerLives : MonoBehaviour
{
    public int lives = 3;
    public GameObject explosionPrefab;
    public Image[] livesUi;
    public GameObject gameOverPanel;
    public GameObject playerShip; // Referência ao GameObject do jogador

    void Start()
    {
        UpdateLivesUI();
        gameOverPanel.SetActive(false); // Garante que o painel de Game Over está desativado no início
    }

    private void Update()
    {
        if (lives <= 0)
        {
            GameOver();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.tag == "Enemy")
        {
            Destroy(collision.collider.gameObject);
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            lives -= 1;
            for (int i = 0; i < livesUi.Length; i++)
            {
                if (i < lives)
                {
                    livesUi[i].enabled = true;
                }
                else
                {
                    livesUi[i].enabled = false;
                }
            }

            if (lives <= 0)
            {
                Destroy(gameObject);
                GameOver();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy Projectile")
        {
            Destroy(collision.gameObject);
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            lives -= 1;
            for (int i = 0; i < livesUi.Length; i++)
            {
                if (i < lives)
                {
                    livesUi[i].enabled = true;
                }
                else
                {
                    livesUi[i].enabled = false;
                }
            }

            if (lives <= 0)
            {
                Destroy(gameObject);
                GameOver();
            }
        }
    }

    public void GainLife()
    {
        // Verifica se o jogador tem menos de 5 vidas
        if (lives < 5)
        {
            lives++; // Aumenta a vida do jogador
            UpdateLivesUI();
        }
    }

    void GameOver()
    {
        gameOverPanel.SetActive(true); // Mostra o painel de Game Over
        // Aqui vamos desativar os elementos em vez de destruí-los
        DisableGameElements();
        Time.timeScale = 0; // Pausa o jogo
    }

    void DisableGameElements()
    {
        // Desativa o jogador
        if (playerShip != null)
            playerShip.SetActive(false);

        // Desativa os inimigos
        LevelManager levelManager = FindObjectOfType<LevelManager>();
        if (levelManager != null && levelManager.currentShipsInstance != null)
        {
            levelManager.currentShipsInstance.SetActive(false);
        }

        // Desativa outros componentes do jogo, como spawners de projéteis, se necessário
    }

    
    void UpdateLivesUI()
    {
        for (int i = 0; i < livesUi.Length; i++)
        {
            livesUi[i].enabled = i < lives;
        }
    }
}

