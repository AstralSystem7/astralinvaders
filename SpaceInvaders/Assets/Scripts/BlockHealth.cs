using UnityEngine;

public class BlockHealth : MonoBehaviour
{
    public GameObject asteroideexpPrefab;
    public int maxHealth = 4; // Vida máxima do bloco
    private int currentHealth; // Vida atual do bloco
    public Sprite[] damageSprites; // Array de sprites para cada estado de dano

    private SpriteRenderer spriteRenderer; // Referência para o SpriteRenderer do bloco

    void Start()
    {
        currentHealth = maxHealth; // Inicializa a vida atual com a vida máxima
        spriteRenderer = GetComponent<SpriteRenderer>(); // Obtém o SpriteRenderer do bloco
        UpdateBlockAppearance(); // Atualiza a aparência inicial do bloco
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // Verifica se o bloco foi atingido por um projétil
        if (other.gameObject.tag == "Projectile")
        {
            TakeDamage(1); // Chama a função de dano
            Destroy(other.gameObject); // Destrói o projétil
        }
        else if (other.gameObject.tag == "Enemy Projectile")
        {
            TakeDamage(1); // Chama a função de dano
            Destroy(other.gameObject); // Destrói o projétil
        }
    }

    void TakeDamage(int damage)
    {
        currentHealth -= damage; // Reduz a vida do bloco

        // Verifica se a vida do bloco chegou a zero
        if (currentHealth <= 0)
        {
            Instantiate(asteroideexpPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject); // Destrói o bloco
        }
        else
        {
            // Atualiza a aparência do bloco para refletir o dano recebido
            UpdateBlockAppearance();
        }
    }

    void UpdateBlockAppearance()
    {
        // Altera o sprite do bloco com base na vida atual
        // Assumindo que o array damageSprites é configurado com sprites para Asteroide1, Asteroide2, e Asteroide3, respectivamente
        switch (currentHealth)
        {
            case 3:
                spriteRenderer.sprite = damageSprites[0]; // Vida cheia
                break;
            case 2:
                spriteRenderer.sprite = damageSprites[1]; // Dano moderado
                break;
            case 1:
                spriteRenderer.sprite = damageSprites[2]; // Dano severo
                break;
        }
    }
}