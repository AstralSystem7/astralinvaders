using UnityEngine;
using UnityEngine.SceneManagement; // Necessário para carregar cenas

public class GameOverManager : MonoBehaviour
{
    public void RestartGame()
    {
        Time.timeScale = 1; // Restaura o tempo do jogo
        LevelManager levelManager = FindObjectOfType<LevelManager>();
        if (levelManager != null)
        {
            levelManager.ResetLevel(); // Reset do nível
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // É chamado quando o botão de sair é pressionado
    public void QuitGame()
    {
        // Restaura o tempo do jogo para normal
        Time.timeScale = 1; // Isso garante que qualquer coisa que dependa do tempo volte a se mover
        // Fecha o jogo
        Application.Quit();
#if UNITY_EDITOR
        // Se estiver no editor da Unity, pare a reprodução
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
    
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu"); // Certifique-se de que o nome da cena está correto
    }
}