using UnityEngine;

public class SpecialEnemySpawner : MonoBehaviour
{
    public GameObject bonusShip; // Referência para o prefab BonusShip
    public float spawnMax = 120f; // Tempo máximo para o próximo spawn
    public float spawnMin = 45f; // Tempo mínimo para o próximo spawn
    private float spawnTimer; // Temporizador para controlar o spawn
    public Vector2 spawnPosition = new Vector2((float)-7.8, 4); // Posição inicial da nave especial
    public float moveSpeed = 1f; // Velocidade de movimento da nave especial

    void Start()
    {
        // Define o temporizador para um valor aleatório inicial
        spawnTimer = Random.Range(spawnMin, spawnMax);
    }

    void Update()
    {
        // Decrementa o temporizador com base no tempo passado
        spawnTimer -= Time.deltaTime;

        // Verifica se é hora de spawnar a nave especial
        if (spawnTimer <= 0)
        {
            // Reseta o temporizador
            spawnTimer = Random.Range(spawnMin, spawnMax);
            
            // Instancia a nave especial em uma posição definida, usualmente à esquerda da área de jogo
            GameObject enemy = Instantiate(bonusShip, new Vector3(spawnPosition.x, spawnPosition.y, 0), Quaternion.identity);
            
            // Adiciona o script de movimento à nave especial
            MoveRight moveRightScript = enemy.AddComponent<MoveRight>();
            moveRightScript.speed = moveSpeed;
        }
    }
}

// Classe separada para controlar o movimento da nave especial
public class MoveRight : MonoBehaviour
{
    public float speed = 5f;

    void Update()
    {
        // Move a nave especial para a direita
        transform.position += Vector3.right * (speed * Time.deltaTime);
    }
}