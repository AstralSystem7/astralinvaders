using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{
    public float movespeed = (float)0.25;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.right * (Time.deltaTime * movespeed));

    }

    private void OnTriggerEnter2D(Collider2D collision) //Verifica se as naves colidiram com a barreira, inverte o movimento e desce a coluna
    {
        if (collision.gameObject.tag == "Boundary")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 1, transform.position.z);
            movespeed *= -1;
        }
    }
}
