using UnityEngine;
using TMPro;

public class PointManager : MonoBehaviour
{
    public int score;
    public TMP_Text scoreText;
    public PlayerLives playerLives; // Referência ao script PlayerLives

    void Start()
    {
        scoreText.text = "Score:" + score;
    }

    public void UpdateScore(int points)
    {
        int previousScore = score;
        score += points;
        scoreText.text = "Score:" + score;

        // Verifica se a pontuação passou de um múltiplo de 1000
        if (previousScore / 1000 < score / 1000)
        {
            playerLives.GainLife(); // Chama a função para ganhar vida
        }
    }
}