using UnityEngine;

public class ProjectileShoot : MonoBehaviour
{
    public GameObject projectilePrefab;
    private GameObject currentProjectile; // Adiciona esta variável

    void Update()
    {
        // Checa se o botão de disparo foi pressionado e se não há um projétil atualmente ativo
        if (Input.GetButtonDown("Fire1") && currentProjectile == null)
        {
            // Instancia o projétil e guarda a referência no currentProjectile
            currentProjectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
            // Configura este script como pai para permitir a limpeza posterior
            Projectile projectileScript = currentProjectile.GetComponent<Projectile>();
            if (projectileScript != null)
            {
                projectileScript.SetParent(this);
            }
        }
    }

    // Método chamado pelo projétil quando ele é destruído ou sai da tela
    public void ClearProjectile()
    {
        currentProjectile = null;
    }
}